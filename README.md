[![Project Status: Wip - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/0.1.0/wip.svg)](http://www.repostatus.org/#wip)

# autonomics.find

Statistic.  Part of the [*autonomics*](https://bitbucket.org/account/user/graumannlabtools/projects/autonomics) suite of packages.

## Installation

To install the package, you first need the
[*devtools*](https://github.com/hadley/devtools) package.

```{r}
install.packages("devtools")
```

Then you can install the *autonomics.find* package using

```{r}
library(devtools)
install_bitbucket("graumannlabtools/autonomics.find")
```

## Functionality

`add_limma_0` runs a limma analysis comparing each of the subgroups to zero.

`calc_sig_a` and `calc_sig_b` calculate MaxQuant-style significance values on a vector.

`add_sig_b` calls `calc_sig_b` on each column of a `ProtSet`'s expression matrix and adds the results to the `sig_b` slot of the object.

`plot_feature_bars` draws a barplot of the log2 ratios by feature.  `plot_top_feature_bars` does the same, but restricts the content to the most significant features.

`plot_feature_profiles` draws a scatter plot of a `ProtSet`'s expression matrix by sample.

`assert_is_valid_limma_contrast` checks a character vector of contrasts for validity with a `limma` model.



